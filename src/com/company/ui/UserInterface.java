package com.company.ui;

import com.company.domain.Friendship;
import com.company.domain.Tuple;
import com.company.domain.User;
import com.company.domain.validators.ValidationException;
import com.company.service.FriendshipService;
import com.company.service.UserService;


import java.util.Scanner;

public class UserInterface {
    UserService userService;
    FriendshipService friendshipService;

    public UserInterface(UserService userService, FriendshipService friendshipService) {
        this.userService = userService;
        this.friendshipService = friendshipService;

    }

    public void console() {
        boolean ok = true;
        while (ok) {
            System.out.println("""
                    1.Add an user
                    2.Remove an user
                    12.Show all users
                    3.Add a friendship
                    4.Remove a friendship
                    34.Show all friendships
                    7.Exit""");
            System.out.println("Choose an option");
            Scanner in = new Scanner(System.in);
            int opt = Integer.parseInt(in.nextLine());
            switch (opt) {
                case 1 -> {
                    System.out.println("First name:");
                    String firstname = in.nextLine();
                    System.out.println("Last name:");
                    String lastname = in.nextLine();
                    User user = new User(lastname, firstname);

                    try {
                        userService.add(user);
                    } catch (ValidationException e) {
                        System.out.println(e);
                    }
                }
                case 2 -> {

                    System.out.println("Id:");
                    long id = Long.parseLong(in.nextLine());
                    try {
                        userService.delete(id);
                    } catch (ValidationException e) {
                        System.out.println(e);
                    }
                }
                case 12 -> {
                    userService.getAll().forEach(System.out::println);
                }
                case 3 -> {
                    System.out.println("user1 id: ");
                    Long fid = Long.parseLong(in.nextLine());
                    System.out.println("user2 id: ");
                    Long sid = Long.parseLong(in.nextLine());
                    Tuple<Long, Long> ship = new Tuple<>(fid, sid);
                    long millis = System.currentTimeMillis();
                    java.sql.Date date = new java.sql.Date(millis);
                    Friendship friendship = new Friendship(ship, date);
                    try {
                        friendshipService.add(friendship);
                    } catch (ValidationException e) {
                        System.out.println(e);
                    }
                }
                case 4 -> {
                    System.out.println("Id");
                    long id = Long.parseLong(in.nextLine());
                    try {
                        friendshipService.removeFriendship(id);
                    } catch (ValidationException | IllegalArgumentException e) {
                        System.out.println(e);
                    }

                }
                case 34 -> {
                    friendshipService.getAll().forEach(System.out::println);
                }

                case 7 -> {
                    System.out.println("Bye!");
                    ok = false;
                }
                default -> {
                    System.out.println("Invalid option");
                }
            }
        }

    }
}
