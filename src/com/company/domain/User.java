package com.company.domain;


import java.util.ArrayList;
import java.util.List;

public class User extends Entity<Long> {
    private final String firstName;
    private final String lastName;
    private final List<User> friendsList = new ArrayList<>();

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * @return the firstName of a user
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @return the lastName of a user
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @return the list of friends of a user
     */
    public List<User> getFriendsList() {
        return friendsList;
    }


    /**
     * @param friend is added to the friendlist
     */
    public void addFriend(User friend) {
        this.friendsList.add(friend);
    }

    public void deleteFriend(User u) {
        this.friendsList.remove(u);
    }


    public String info() {
        return "userId =  "+ id+
                ", firstName = " + firstName +
                ", lastName = " + lastName ;
    }

    @Override
    public String toString() {

        StringBuilder result = new StringBuilder();
        result.append(this.info());
        result.append("\n");
        result.append("Friends:");
        result.append("\n");
        for(User user: friendsList){
            result.append(user.info());
            result.append("\n");
        }
        result.append("\n");
        return result.toString();

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User that)) return false;
        return getFirstName().equals(that.getFirstName()) &&
                getLastName().equals(that.getLastName()) &&
                getFriendsList().equals(that.getFriendsList());
    }

}

