package com.company.domain;

import java.util.Objects;

public class Tuple<Entity1, Entity2> {
    private Entity1 entity1;
    private Entity2 entity2;

    public Tuple(Entity1 entity1, Entity2 entity2) {
        this.entity1 = entity1;
        this.entity2 = entity2;
    }

    /**
     * @return the first element from tuple
     */
    public Entity1 getE1() {
        return entity1;
    }

    /**
     * @return the second element from tuple
     */
    public Entity2 getE2() {
        return entity2;
    }

    /**
     * @param entity1 set the first element from tuple
     */
    public void setE1(Entity1 entity1) {
        this.entity1 = entity1;
    }

    /**
     * @param entity2 set the second element from tuple
     */
    public void setE2(Entity2 entity2) {
        this.entity2 = entity2;
    }

    @Override
    public String toString() {
        return "" + entity1 + "," + entity2;
    }

    @Override
    public boolean equals(Object obj) {
        return this.entity1.equals(((Tuple) obj).entity1) && this.entity2.equals(((Tuple) obj).entity2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entity1, entity2);
    }
}

