package com.company.domain.validators;

public interface Validator<T> {

    /**
     * @throws ValidationException if the entity is not valid
     */
    void validate(T entity) throws ValidationException;
}
