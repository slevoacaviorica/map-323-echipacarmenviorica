package com.company.service;

import com.company.domain.*;
import com.company.domain.validators.ValidationException;
import com.company.repository.Repository;
import com.company.repository.db.FriendshipDbRepository;
import com.company.repository.db.UserDbRepository;

public class UserService {

    UserDbRepository repoUser;
    FriendshipDbRepository repoFriendship;

    public UserService(Repository<Long, User> repoUser, Repository<Long, Friendship> repoFriendship) {
        this.repoUser = (UserDbRepository) repoUser;
        this.repoFriendship = (FriendshipDbRepository) repoFriendship;
        ((FriendshipDbRepository) repoFriendship).setFriendships();
    }

    /**
     * @param entity must be not null
     */
    public void add(User entity) {
        repoUser.save(entity);
    }

    /**
     * @param id must be not null
     */
    public void delete(Long id) {
        if (repoUser.findOne(id) == null)
            throw new ValidationException("Does not exist!");
        repoFriendship.removeallFriendships(repoUser.findOne(id));
        repoUser.delete(id);
    }

    public Iterable<User> getAll() {
        return repoUser.findAll();
    }

    public User getById(Long id) {
        return this.repoUser.findOne(id);
    }
}




