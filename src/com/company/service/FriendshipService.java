package com.company.service;

import com.company.domain.Friendship;
import com.company.domain.User;
import com.company.domain.validators.ValidationException;
import com.company.repository.Repository;
import com.company.repository.db.FriendshipDbRepository;
import com.company.repository.db.UserDbRepository;

public class FriendshipService {
    UserDbRepository repoUser;
    FriendshipDbRepository repoFriendship;


    public FriendshipService(Repository<Long, User> repoUser, Repository<Long, Friendship> repoFriendship) {
        this.repoUser = (UserDbRepository) repoUser;
        this.repoFriendship = (FriendshipDbRepository) repoFriendship;
    }

    /**
     * @param entity must be not null
     */
    public void add(Friendship entity) {
        Long id1 = entity.getE1();
        Long id2 = entity.getE2();

        if (repoUser.findOne(id1) == null || repoUser.findOne(id2) == null)
            throw new ValidationException("Id is invalid");

        repoUser.findOne(id1).addFriend(repoUser.findOne(id2));
        repoUser.findOne(id2).addFriend(repoUser.findOne(id1));
        long millis = System.currentTimeMillis();
        java.sql.Date date = new java.sql.Date(millis);
        entity.setDate(date);
        repoFriendship.save(entity);
    }

    /**
     * @param id must be not null
     */
    public void removeFriendship(Long id) {
        if (repoFriendship.findOne(id) == null)
            throw new ValidationException("Nu exista");
        repoFriendship.delete(id);
    }

    public Iterable<Friendship> getAll() {
        return repoFriendship.findAll();
    }


}

