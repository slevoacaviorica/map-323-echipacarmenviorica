package com.company.repository.db;

import com.company.domain.Friendship;
import com.company.domain.Tuple;
import com.company.domain.User;
import com.company.repository.Repository;

import java.sql.*;
import java.sql.Date;
import java.util.*;

public class FriendshipDbRepository implements Repository<Long, Friendship> {
    private final String url;
    private final String username;
    private final String password;
    UserDbRepository repoUser;

    public FriendshipDbRepository(String url, String username, String password, Repository<Long, User> repoUser) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.repoUser = (UserDbRepository) repoUser;
    }

    @Override
    public Friendship findOne(Long aLong) {
        List<Friendship> friendships = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friendships where id = ?")) {
            statement.setLong(1, aLong);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                Long user1_id = resultSet.getLong("user1_id");
                Long user2_id = resultSet.getLong("user2_id");
                Date date = resultSet.getDate("date");
                Tuple<Long, Long> ship = new Tuple<>(user1_id, user2_id);
                Friendship friendship = new Friendship(ship, date);
                friendship.setId(id);
                friendships.add(friendship);
            }
            if (friendships.size() > 0)
                return friendships.get(0);
            else
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Friendship> findAll() {
        Set<Friendship> friendships = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friendships");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                Long user1_id = resultSet.getLong("user1_id");
                Long user2_id = resultSet.getLong("user2_id");
                Date date = resultSet.getDate("date");
                //LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                Tuple<Long, Long> ship = new Tuple<>(user1_id, user2_id);
                Friendship friendship = new Friendship(ship, date);
                friendship.setId(id);
                friendships.add(friendship);
            }
            return friendships;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return friendships;
    }

    @Override
    public Friendship save(Friendship entity) {

        String sql = "insert into friendships (user1_id, user2_id, date) values (?, ?, ?)";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, entity.getE1());
            ps.setLong(2, entity.getE2());
            ps.setDate(3, entity.getDate());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Friendship remove(Friendship entity) {
        return null;
    }

    @Override
    public void delete(Long aLong) {
        String sql = "delete from friendships where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, aLong);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeallFriendships(User user) {
        for (Friendship friendship : findAll()) {
            if (Objects.equals(friendship.getE1(), user.getId()) || Objects.equals(friendship.getE2(), user.getId())) {
                delete(friendship.getId());
            }
        }
    }

    public void setFriendships() {
        for (Friendship friendship : findAll()) {
            repoUser.findOne(friendship.getE1()).addFriend(repoUser.findOne(friendship.getE2()));
            repoUser.findOne(friendship.getE2()).addFriend(repoUser.findOne(friendship.getE1()));
        }
    }

}

