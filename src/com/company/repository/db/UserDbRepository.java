package com.company.repository.db;

import com.company.domain.User;
import com.company.domain.validators.Validator;
import com.company.repository.Repository;

import java.sql.*;
import java.util.*;

public class UserDbRepository implements Repository<Long, User> {
    private final String url;
    private final String username;
    private final String password;
    private Validator<User> validator;


    public UserDbRepository(String url, String username, String password, Validator<User> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    /*
        @Override
        public User findOne(Long aLong) {

            List<User> users = new ArrayList<>();
            try (Connection connection = DriverManager.getConnection(url, username, password);
                 PreparedStatement statement = connection.prepareStatement("SELECT * from users where id = ?")) {
                statement.setLong(1, aLong);
                ResultSet resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    Long id = resultSet.getLong("id");
                    String firstName = resultSet.getString("first_name");
                    String lastName = resultSet.getString("last_name");
                    User user = new User(firstName, lastName);
                    user.setId(id);
                    users.add(user);
                }
                if (users.size() > 0)
                    return users.get(0);
                else
                    return null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }*/
    @Override
    public User findOne(Long aLong) {
        String sql = "SELECT * from users WHERE id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
        ) {

            statement.setLong(1, aLong);
            ResultSet resultSet = statement.executeQuery();

            resultSet.next();
            Long id = resultSet.getLong("id");
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");

            User user = new User(firstName, lastName);
            user.setId(id);

            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<User> findAll() {
        Set<User> users = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from users");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");

                User user = new User(firstName, lastName);
                user.setId(id);
                users.add(user);
                String sql2 = "SELECT user1_id FROM friendships WHERE user2_id = ?";
                PreparedStatement statement2 = connection.prepareStatement(sql2);
                statement2.setLong(1, id);
                ResultSet resultSet2 = statement2.executeQuery();

                while (resultSet2.next()) {
                    User user2 = findOne(resultSet2.getLong(1));
                    user2.setId(resultSet2.getLong(1));
                    user.addFriend(user2);
                }

                String sql3 = "SELECT user2_id FROM friendships WHERE user1_id = ?";
                PreparedStatement statement3 = connection.prepareStatement(sql3);
                statement3.setLong(1, id);
                ResultSet resultSet3 = statement3.executeQuery();

                while (resultSet3.next()) {
                    User user3 = findOne(resultSet3.getLong(1));
                    user3.setId(resultSet3.getLong(1));
                    user.addFriend(user3);
                }

            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public User save(User entity) {

        String sql = "insert into users (first_name, last_name) values (?, ?)";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, entity.getFirstName());
            ps.setString(2, entity.getLastName());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void delete(Long aLong) {
        String sql = "delete from users where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, aLong);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public User remove(User entity) {
        return null;
    }

}
