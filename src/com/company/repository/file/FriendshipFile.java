package com.company.repository.file;

import com.company.domain.Friendship;
import com.company.domain.Tuple;
import com.company.domain.User;
import com.company.domain.validators.Validator;

import java.sql.Date;
import java.util.List;
import java.util.Objects;

public class FriendshipFile extends AbstractFileRepository<Long, Friendship> {

    UserFile repoUser;

    public FriendshipFile(Validator<Friendship> validator, String fileName, UserFile repoUser) {
        super(validator, fileName);
        this.repoUser = repoUser;
    }

    @Override
    public Friendship extractEntity(List<String> attributes, Date date) {
        Tuple<Long, Long> ship = new Tuple<>(Long.parseLong(attributes.get(0)), Long.parseLong(attributes.get(1)));
        Friendship friendship = new Friendship(ship, date);


        Long id = friendship.getId();
        friendship.setId(id);


        return friendship;
    }

    @Override
    protected String createEntityAsString(Friendship entity) {
        return entity.getE1() + ";" + entity.getE2() + ";" + entity.getDate();
    }


    public void removeallFriendships(User user) {
        for (Friendship friendship : findAll()) {
            if (Objects.equals(friendship.getE1(), user.getId()) || Objects.equals(friendship.getE2(), user.getId())) {
                remove(friendship);
            }
        }
    }

    public void setFriendships() {
        for (Friendship friendship : findAll()) {
            repoUser.findOne(friendship.getE1()).addFriend(repoUser.findOne(friendship.getE2()));
            repoUser.findOne(friendship.getE2()).addFriend(repoUser.findOne(friendship.getE1()));
        }
    }
}
