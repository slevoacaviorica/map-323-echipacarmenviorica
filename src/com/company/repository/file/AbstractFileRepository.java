package com.company.repository.file;

import com.company.domain.Entity;
import com.company.domain.validators.Validator;
import com.company.repository.inmemory.InMemoryRepository;

import java.io.*;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractFileRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID, E> {

    private final String fileName;

    public AbstractFileRepository(Validator<E> validator, String fileName) {
        super(validator);
        this.fileName = fileName;
        loadData();
    }

    /**
     * load data from file to memory
     */
    private void loadData() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                List<String> atributes = Arrays.asList(line.split(";"));
                long millis = System.currentTimeMillis();
                java.sql.Date date = new java.sql.Date(millis);
                E element = extractEntity(atributes, date);
                super.save(element);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Fisierul nu a fost gasit!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Eroare la citire!");
            e.printStackTrace();
        }
    }

    /**
     * @return an entity
     */
    public abstract E extractEntity(List<String> attributes, Date date);

    /**
     * @return a string
     */
    protected abstract String createEntityAsString(E entity);

    @Override
    public E save(E entity) {
        E result = super.save(entity);
        if (result == null)
            writeToFileOne(entity);
        return result;
    }

    /**
     * write an entity to file
     *
     * @param entity most not be null
     */
    protected void writeToFileOne(E entity) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));) {
            writer.write(createEntityAsString(entity));
            writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * write the data from memory to file
     */
    protected void writeToFile() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false))) {
            for (E e : this.findAll()) {
                writer.write(createEntityAsString(e));
                writer.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public E remove(E entity) {
        E result = super.remove(entity);
        writeToFile();
        return result;
    }

}

