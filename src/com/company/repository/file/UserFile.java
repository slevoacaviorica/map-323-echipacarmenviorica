package com.company.repository.file;

import com.company.domain.User;
import com.company.domain.validators.Validator;

import java.sql.Date;
import java.util.List;

public class UserFile extends AbstractFileRepository<Long, User> {

    public UserFile(Validator<User> validator, String fileName) {
        super(validator, fileName);
    }

    @Override
    public User extractEntity(List<String> attributes, Date date) {
        User user = new User(attributes.get(1), attributes.get(2));
        user.setId(Long.parseLong(attributes.get(0)));
        return user;
    }

    @Override
    protected String createEntityAsString(User entity) {
        return entity.getId() + ";" + entity.getFirstName() + ";" + entity.getLastName();
    }

}

