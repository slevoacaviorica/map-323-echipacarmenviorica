package com.company;

import com.company.domain.Friendship;
import com.company.domain.User;
import com.company.domain.validators.UserValidator;
import com.company.repository.Repository;
import com.company.repository.db.FriendshipDbRepository;
import com.company.repository.db.UserDbRepository;
import com.company.service.FriendshipService;
import com.company.service.UserService;
import com.company.ui.UserInterface;

public class Main {

    public static void main(String[] args) {

        Repository<Long, User> userRepoDb = new UserDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres", "nucadecocos", new UserValidator());
        Repository<Long, Friendship> friendshipRepoDb = new FriendshipDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres", "nucadecocos", userRepoDb);
        UserService userService = new UserService(userRepoDb, friendshipRepoDb);
        FriendshipService friendshipService = new FriendshipService(userRepoDb, friendshipRepoDb);
        UserInterface userInterface = new UserInterface(userService, friendshipService);
        userInterface.console();

    }
}

